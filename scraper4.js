const puppeteer = require('puppeteer');
const {
  promises: { writeFile },
} = require('fs');

(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  const productUrl = 'https://www.adorecosmetics.com/collections/skin-care?page=1';

  const productData = [];
  const nextPageSelector = '.pagination-custom li:nth-child(2)';
  await page.goto(productUrl, { waitUntil: 'networkidle2' });
  // page.on('console', (msg) => console.log(msg.text()));

  let goToNextPage = true;

  try {
    while (goToNextPage) {
      console.log('next');
      await page.waitForSelector('.grid__item img');

      const { products, nextPage } = await page.evaluate(
        ({ nextPageSelector }) => {
          const productElms = [...document.querySelectorAll('main .grid-uniform .grid__item')]; //select everything on the page i think you should remove **main**
          const parent = document.querySelector(nextPageSelector);
          const exists = parent.className === 'disabled' ? undefined : (parent.querySelector('a') || {}).href;
          console.log(nextPageSelector, !!exists);
          const products = productElms.map((el) => {
            // const caption = el.querySelector('.prod-caption > a');
            const image = (el.querySelector('img') || {}).src;
            const name = (el.querySelector('.grid-link__title') || {}).innerText;
            const price = (el.querySelector('.grid-link__meta') || {}).innerText;
            const desc = (el.querySelector('a') || {}).href;
            return { image, name, price, desc };
          });
          return { products, nextPage: exists };
        },
        { nextPageSelector }
      );

      productData.push(...products);
      goToNextPage = !!nextPage;
      if (goToNextPage) {
        console.log('navigating to:', nextPage);
        await Promise.all([page.click(nextPageSelector), page.waitForNavigation()]);
      }
    }
  } catch ({ message }) {
    console.log(message);
  }
  await page.close();

  console.log('fetching descriptions...');

  for (const item of productData) {
    const { desc } = item;
    const page = await browser.newPage();
    await page.goto(desc, { waitUntil: 'networkidle2' });
    try {
      item.desc = await page.evaluate(() => document.querySelector('.product_data_wrapper').innerHTML);
    } catch (error) {
      console.log(error.message);
    }
    page.close();
  }

  await writeFile('products4b.json', JSON.stringify(productData));
  await browser.close();
  process.exit();
})();
