const puppeteer = require('puppeteer');
const {
  promises: { writeFile },
} = require('fs');

(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  const productUrl = 'https://cocoagecosmetics.com/collections/all?page=1';

  const productData = [];
  const nextPageSelector = '#pagination > .current + a';
  await page.goto(productUrl, { waitUntil: 'networkidle2' });

  let goToNextPage = true;

  while (goToNextPage) {
    console.log('next');
    await page.waitForSelector('.reveal img');
    const { products, nextPage } = await page.evaluate(
      ({ nextPageSelector }) => {
        try {
          const productElms = [...document.querySelectorAll('.product-loop > .product-index')]; //select everything on the page
          const exists = (document.querySelector(nextPageSelector) || {}).href;
          console.log(nextPageSelector, !!exists);
          const products = productElms.map((el) => {
            // const caption = el.querySelector('.prod-caption > a');
            const image = el.querySelector('.reveal img').innerText;
            const name = el.querySelector('.product-info > div > a > .product-title').innerText;
            const price = el.querySelector('.product-info .product-info-inner .price').innerText;
            const desc = el.querySelector('.product-info .product-info-inner > a').href;
            return { image, name, price, desc };
          });
          return { products, nextPage: exists };
        } catch (error) {
          console.log(error.message);
        }
      },
      { nextPageSelector }
    );
    productData.push(...products);
    goToNextPage = !!nextPage;
    if (goToNextPage) {
      console.log('navigating to:', nextPage);
      await Promise.all([page.click(nextPageSelector), page.waitForNavigation()]);
    }
  }
  await page.close();

  console.log('fetching descriptions...');

  for (const item of productData) {
    const { desc } = item;
    const page = await browser.newPage();
    await page.goto(desc, { waitUntil: 'networkidle2' });
    item.desc = await page.evaluate(() => document.querySelector('.desc').innerHTML);
    await page.close();
  }

  await writeFile('products2.json', JSON.stringify(productData));
  await browser.close();
  process.exit();
})();
