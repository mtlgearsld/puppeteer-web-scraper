const puppeteer = require('puppeteer');

let productUrl = 'https://www.deepseacosmetics.com/collections/all';
(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({ width: 1920, height: 926 });
  await page.goto(productUrl, { waitUntil: 'networkidle2' });

  let productData = await page.evaluate(() => {
    let products = [];
    let productElms = document.querySelectorAll('.product-list > div[class*=prod-block]'); //select everything on the page
    productElms.forEach((productelement) => {
      let productJson = {};
      try {
        productJson.image = productelement.querySelector('div[class="prod-image-wrap"] > a > img').innerText;
        productJson.name = productelement.querySelector('div[class="prod-caption"] > a > div[class="title"]').innerText;
        productJson.price = productelement.querySelector('div[class="prod-caption"] > a > span[class="prod-price"]').innerText;
        productJson.desc = productelement.querySelector('div[class=prod-image-wrap] > a').href;
      } catch (exception) {}
      products.push(productJson);
    });
    return products;
  });

  console.dir(productData);
  await page.close();
  console.log('fetching descriptions...');

  for (const item of productData) {
    const { desc } = item;
    const page = await browser.newPage();
    await page.goto(desc, { waitUntil: 'networkidle2' });
    item.desc = await page.evaluate(() => document.querySelector('#product-description').innerHTML);
    await page.close();
  }
  console.log(productData);

  await browser.close();
  process.exit();
})();
