const puppeteer = require('puppeteer');
const {
  promises: { writeFile },
} = require('fs');

(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  const productUrl = 'https://www.deepseacosmetics.com/collections/all';

  const productData = [];
  const nextPageSelector = '.pagination .page.current + .page > a';
  await page.goto(productUrl, { waitUntil: 'networkidle2' });
  // page.on('console', (msg) => console.log(msg.text()));

  let goToNextPage = true;

  while (goToNextPage) {
    console.log('next');
    await page.waitForSelector('.prod-image-wrap img');

    const { products, nextPage } = await page.evaluate(
      ({ nextPageSelector }) => {
        try {
          const productElms = [...document.querySelectorAll('.product-list .prod-block')]; //select everything on the page
          const exists = (document.querySelector(nextPageSelector) || {}).href;
          console.log(nextPageSelector, !!exists);

          const products = productElms.map((el) => {
            const caption = el.querySelector('.prod-caption > a');
            const image = el.querySelector('.prod-image-wrap img').innerText;
            const name = caption.querySelector('.title').innerText;
            const price = caption.querySelector('.prod-price').innerText;
            const desc = el.querySelector('.prod-image-wrap > a').href;
            return { image, name, price, desc };
          });
          return { products, nextPage: exists };
        } catch (error) {
          console.log(error.message);
        }
      },
      { nextPageSelector }
    );
    productData.push(...products);
    goToNextPage = !!nextPage;
    if (goToNextPage) {
      console.log('navigating to:', nextPage);
      await Promise.all([page.click(nextPageSelector), page.waitForNavigation()]);
    }
  }
  await page.close();

  console.log('fetching descriptions...');

  for (const item of productData) {
    const { desc } = item;
    const page = await browser.newPage();
    await page.goto(desc, { waitUntil: 'networkidle2' });
    item.desc = await page.evaluate(() => document.querySelector('#product-description').innerHTML);
    await page.close();
  }

  await writeFile('products1.json', JSON.stringify(productData));
  await browser.close();
  process.exit();
})();
