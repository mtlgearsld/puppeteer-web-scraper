const puppeteer = require('puppeteer');
const {
  promises: { writeFile },
} = require('fs');

let productUrl = 'https://techturebeauty.com/collections/products';
(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.setViewport({ width: 1920, height: 926 });
  await page.goto(productUrl, { waitUntil: 'networkidle2' });

  let productData = await page.evaluate(() => {
    let products = [];
    let productElms = document.querySelectorAll('.collection-template .product-index'); //select everything on the page
    productElms.forEach((productelement) => {
      let productJson = {};
      try {
        productJson.image = productelement.querySelector('.prod-image img').src;
        productJson.name = productelement.querySelector('.product-title').innerText;
        productJson.price = productelement.querySelector('.prod-price').innerText;
        productJson.desc = productelement.querySelector('.prod-image > a').href;
      } catch (exception) {}
      products.push(productJson);
    });
    return products;
  });

  console.dir(productData);
  await page.close();
  console.log('fetching descriptions...');

  for (const item of productData) {
    const { desc } = item;
    const page = await browser.newPage();
    await page.goto(desc, { waitUntil: 'networkidle2' });
    item.desc = await page.evaluate(() => document.querySelector('.product-details > li').innerHTML);
    await page.close();
  }
  console.log(productData);

  await writeFile('products3a.json', JSON.stringify(productData));
  await browser.close();
  process.exit();
})();
